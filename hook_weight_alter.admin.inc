<?php
/**
 * @file
 * Administration functions file.
 */

/**
 * Page callback: Form constructor for the hook weight alter admin list.
 */
function hook_weight_alter_admin($form, &$form_state) {
  drupal_add_library('system', 'drupal.collapse');
  $form['custom_hook'] = array(
    'custom_hook_name' => array(
      '#type'        => 'textfield',
      '#title'       => 'Hook',
      '#description' => t('You can specify a custom hook if is not in the list below'),
    ),
    'custom_hook_action' => array(
      '#type'        => 'submit',
      '#value'       => 'Edit',
    ),
  );
  $hooks = hook_weight_alter_get_hooks();
  $header = array(t('Hook'), t('Edit'));
  if ($hooks) {
    ksort($hooks);
    foreach ($hooks as $module_name => $group) {
      $form[$module_name] = array(
        '#type'        => 'fieldset',
        '#title'       => $module_name,
        '#collapsible' => TRUE,
        '#collapsed'   => TRUE,
      );
      $rows = array();
      asort($group);
      foreach ($group as $hook) {
        if (count(module_implements($hook)) > 1) {
          $rows[] = array(
            $hook,
            l(t('Edit'), 'admin/config/development/hook-weight-alter/' . $hook),
          );
        }
      }
      $variables = array(
        'header' => $header,
        'rows'   => $rows,
        'empty'  => t('No hooks available.'),
      );
      $form[$module_name]['#children'] = theme('table', $variables);
    }
  }
  return $form;
}

/**
 * Form validation handler for hook_weight_alter_admin().
 *
 * @see hook_weight_alter_admin()
 */
function hook_weight_alter_admin_validate($form, &$form_state) {
  $hook = drupal_array_get_nested_value($form_state, array('values', 'custom_hook_name'));
  if (empty($hook)) {
    form_set_error('custom_hook_name', t('The hook is required'));
    return;
  }
  if (!module_implements($hook)) {
    form_set_error('custom_hook_name', t('The hook %hook is not implemented', array('%hook' => $hook)));
  }
}

/**
 * Form submission handler for hook_weight_alter_admin().
 *
 * @see hook_weight_alter_admin()
 */
function hook_weight_alter_admin_submit($form, &$form_state) {
  $hook = drupal_array_get_nested_value($form_state, array('values', 'custom_hook_name'));
  $form_state['redirect'] = 'admin/config/development/hook-weight-alter/' . $hook;
}


/**
 * Form constructor for the hook weight alter administration form.
 */
function hook_weight_alter_hook_admin($form, &$form_state, $hook) {
  $form['warning'] = array(
    '#type' => 'markup',
    '#markup' => '<div class="messages warning">' . t('Alter the hook execution order may generate trouble, use carefully at your own risk.') . '</div>',
  );
  $form['#tree'] = TRUE;
  $form['hook'] = array(
    '#type' => 'value',
    '#value' => $hook,
  );
  $hooks_order = hook_weight_alter_get_hook_altered($hook);
  $implementations = module_implements($hook);
  $enable_restore_button = FALSE;
  if (empty($hooks_order)) {
    $hooks_order = $implementations;
  }
  else {
    $old_hooks = array_diff($hooks_order, $implementations);
    $hooks_order = array_diff($hooks_order, $old_hooks);
    $new_hooks = array_diff($implementations, $hooks_order);
    if (!empty($new_hooks)) {
      $modules_enabled = system_list('module_enabled');
      foreach ($new_hooks as $new_hook) {
        if (isset($modules_enabled[$new_hook])) {

        }
      }
    }
    $enable_restore_button = TRUE;
  }
  if ($hooks_order) {
    foreach ($hooks_order as $weight => $module) {
      $form['hooks'][$module]['module'] = array(
        '#type' => 'markup',
        '#markup' => $module,
      );
      $form['hooks'][$module]['weight'] = array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
      );
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save changes'),
    );
    if ($enable_restore_button) {
      $form['restore'] = array(
        '#type' => 'submit',
        '#value' => t('Restore default'),
      );
    }
  }

  return $form;
}

/**
 * Form submission handler for hook_weight_alter_hook_admin().
 *
 * @see hook_weight_alter_hook_admin()
 */
function hook_weight_alter_hook_admin_submit($form, &$form_state) {
  $hook = drupal_array_get_nested_value($form_state, array('values', 'hook'));
  $hooks = drupal_array_get_nested_value($form_state, array('values', 'hooks'));
  db_delete('hook_weight_alter')
  ->condition('hook', $hook)
  ->execute();

  if ($form_state['values']['op'] === t('Save changes')) {
    foreach ($hooks as $module => $weight) {
      $record = new stdClass();
      $record->hook = $hook;
      $record->module = $module;
      $record->weight = $weight['weight'];
      $record->status = 1;
      drupal_write_record('hook_weight_alter', $record);
    }
  }
  module_implements($hook, FALSE, TRUE);
}

/**
 * Returns the page title of the hook weight alter administration form.
 *
 * @param string $hook
 *   The hook name.
 *
 * @return string
 *   The title of the hook weight alter administration form.
 */
function hook_weight_alter_page_title($hook) {
  return "Manage hook $hook weight";
}

/**
 * Theme function for hook_weight_alter_hook_admin().
 */
function theme_hook_weight_alter_hook_admin($variables) {
  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form['hooks']) as $module) {
    $form['hooks'][$module]['weight']['#attributes']['class'] = array('hooks-order-weight');
    $rows[] = array(
      'data' => array(
        drupal_render($form['hooks'][$module]['module']),
        drupal_render($form['hooks'][$module]['weight']),
      ),
      'class' => array('draggable'),
    );
  }

  $header = array(t('Module'), t('Weight'));
  $vars = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'hooks-order'),
  );
  $output = drupal_render($form['warning']);
  $output .= theme('table', $vars);
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('hooks-order', 'order', 'sibling', 'hooks-order-weight');

  return $output;
}
